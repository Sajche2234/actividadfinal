/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import modelo.NumerosVO;
import modelo.Sesion;
import modelo.UsuarioDAO;
import modelo.UsuarioVO;
import vista.Form_Aritmetica;
import vista.Frm_Escritorio;
import vista.Frm_Login;

/**
 *
 * @author Roberto
 */
public class ControladorLogin implements ActionListener {
    Frm_Login login = new Frm_Login();
    UsuarioVO uvo = new UsuarioVO();
    UsuarioDAO udao = new UsuarioDAO();
    Frm_Escritorio formEsc = new Frm_Escritorio();
    Form_Aritmetica formArit = new Form_Aritmetica();
    NumerosVO num = new NumerosVO();
    Sesion sesion = new Sesion();

    public ControladorLogin(Frm_Login login, UsuarioVO uvo, UsuarioDAO udao, Frm_Escritorio formEsc, Form_Aritmetica formArit, NumerosVO num, Sesion sesion) {
        this.login = login;
        this.uvo = uvo;
        this.udao = udao;
        this.formEsc = formEsc;
        this.formArit = formArit;
        this.num = num;
        this.sesion = sesion;
        login.btnLogin.addActionListener(this);
    }
    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource() == login.btnLogin){            
            uvo.setNick(login.txtUsuario.getText());
            uvo.setPassword(login.txtPassword.getText());
            int resultado = udao.usuario(uvo);
            if(resultado>0){
                uvo.setId_usuario(resultado);
                // Bloque para obtener los datos del usuario.
                for(UsuarioVO usvo : udao.getUsuario(uvo)){
                    sesion.setId_usuario(usvo.getId_usuario());
                    sesion.setNombre_usuario(usvo.getNombre_usuario());
                    sesion.setApellido_usuario(usvo.getApellido_usuario());
                    sesion.setFk_id_tipo_usuario(usvo.getFk_id_tipo_usuario());
                    //usuarios.jcbEstado.setSelectedIndex(usvo.getFk_id_estado());
                }
                udao.bitacora(uvo.getNick(), "El usuario " + uvo.getNick() + " ha iniciado sesión.");
                this.formEsc.jmAdmon.setVisible(false);
                this.formEsc.jmAcciones.setVisible(false);
                this.formEsc.jmPrincipiante.setVisible(false);
                this.formEsc.jmIntermedio.setVisible(false);
                this.formEsc.jmAvanzado.setVisible(false);
                if(sesion.getFk_id_tipo_usuario() == 1) {
                    this.formEsc.jmAdmon.setVisible(true);
                } else if(sesion.getFk_id_tipo_usuario() == 2){
                    this.formEsc.jmAcciones.setVisible(true);
                    this.formEsc.jmPrincipiante.setVisible(true);
                } else if(sesion.getFk_id_tipo_usuario() == 3){
                    this.formEsc.jmAcciones.setVisible(true);
                    this.formEsc.jmIntermedio.setVisible(true);
                } else if(sesion.getFk_id_tipo_usuario() == 4){
                    this.formEsc.jmAcciones.setVisible(true);
                    this.formEsc.jmAvanzado.setVisible(true);
                }
                this.login.dispose();
                this.formEsc.setTitle("BIENVENIDO " + sesion.getNombre_usuario() + " " + sesion.getApellido_usuario());
                this.formEsc.setVisible(true);
                this.formEsc.setLocationRelativeTo(null);
            } else {
                JOptionPane.showMessageDialog(null, "Estas credenciales no se encuentran registradas en la base de datos.");
            }
            //System.out.println("Resultado: " + resultado);
        }
    }
    
}
