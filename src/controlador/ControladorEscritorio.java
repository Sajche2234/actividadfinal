/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Random;
import modelo.NumerosVO;
import modelo.Sesion;
import modelo.UsuarioDAO;
import modelo.UsuarioVO;
import vista.Form_Aritmetica;
import vista.Frm_Adivina;
import vista.Frm_Escritorio;
import vista.Frm_Historia;
import vista.Frm_Login;
import vista.Frm_Traductor;
import vista.Frm_Usuario;

/**
 *
 * @author Roberto
 */
public class ControladorEscritorio implements ActionListener, WindowListener {
    Frm_Escritorio esc = new Frm_Escritorio();
    Form_Aritmetica formArit = new Form_Aritmetica();
    Frm_Historia form_histoty = new Frm_Historia();
    Frm_Adivina adivina = new Frm_Adivina();
    Frm_Traductor traslate = new Frm_Traductor();
    Frm_Usuario user = new Frm_Usuario();
    Frm_Login login = new Frm_Login();
    // Instancias los objetos.
    UsuarioVO uvo = new UsuarioVO();
    UsuarioDAO udao = new UsuarioDAO();
    NumerosVO num = new NumerosVO();
    Sesion sesion = new Sesion();
    public ControladorEscritorio(Frm_Escritorio esc, Frm_Login login, UsuarioVO uvo, UsuarioDAO udao, Form_Aritmetica formArit, NumerosVO num, Sesion sesion, Frm_Historia form_histoty, Frm_Adivina adivina, Frm_Traductor traslate, Frm_Usuario user) {
        this.esc = esc;
        this.login = login;
        this.uvo = uvo;
        this.udao = udao;
        this.formArit = formArit;
        this.adivina = adivina;
        this.traslate = traslate;
        this.user = user;
        this.num = num;
        this.form_histoty = form_histoty;
        this.sesion = sesion;
        esc.jmiSuma.addActionListener(this);
        esc.jmiResta.addActionListener(this);
        esc.jmiMulti.addActionListener(this);
        esc.jmiDiv.addActionListener(this);
        esc.jmiSalir.addActionListener(this);
        esc.jmiHistoria.addActionListener(this);
        esc.jmiAdivinaN.addActionListener(this);
        esc.jmiTraductor.addActionListener(this);
        esc.jmiUsuaios.addActionListener(this);
        esc.jmiLogout.addActionListener(this);
        esc.addWindowListener(this);
        esc.jmiReporte.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource() == esc.jmiLogout){
            this.esc.dispose();
            this.login.txtUsuario.setText("");
            this.login.txtPassword.setText("");
            this.login.setVisible(true);
        }
        if(ae.getSource() == esc.jmiSuma){
           // esc.homeDesktop.add(formArit).setVisible(true);
            this.formArit.setVisible(true);
            this.formArit.setLocationRelativeTo(null);
            Random r = new Random();
            int v1 = r.nextInt(10);
            int v2 = r.nextInt(10);
            num.setN1(v1);
            num.setN2(v2);
            num.setOperacion(1); // Indicando que la operación es una suma.
            num.setIntentos(0);
            String texto = "Calcule la sume entre " + v1 + " y " + v2 + ". ";
            this.formArit.setTitle("BIENVENIDO " + sesion.getNombre_usuario() + " - SUMA");
            this.formArit.lblQuestion.setText(texto);
            this.formArit.txtRespuesta.setText("");
            /*this.esc.homeDesktop.add(formArit);
            formArit.toFront();
            formArit.setVisible(true);*/
        }
        if(ae.getSource() == esc.jmiResta){
            this.formArit.setVisible(true);
            this.formArit.setLocationRelativeTo(null);
            Random r = new Random();
            int v1 = r.nextInt(10);
            int v2 = r.nextInt(10);
            num.setN1(v1);
            num.setN2(v2);
            num.setOperacion(2); // Indicando que la operación es una suma.
            num.setIntentos(0);
            String texto = "Calcule la resta entre " + v1 + " y " + v2 + ". ";
            this.formArit.setTitle("RESTA");
            this.formArit.lblQuestion.setText(texto);
            this.formArit.txtRespuesta.setText("");
        }
        if(ae.getSource() == esc.jmiMulti){
            this.formArit.setVisible(true);
            this.formArit.setLocationRelativeTo(null);
            Random r = new Random();
            int v1 = r.nextInt(10);
            int v2 = r.nextInt(10);
            num.setN1(v1);
            num.setN2(v2);
            num.setOperacion(3); // Indicando que la operación es una suma.
            num.setIntentos(0);
            String texto = "Calcule la multiplicación entre " + v1 + " y " + v2 + ". ";
            this.formArit.setTitle("MULTIPLICACIÓN");
            this.formArit.lblQuestion.setText(texto);
            this.formArit.txtRespuesta.setText("");
        }
        if(ae.getSource() == esc.jmiDiv){
            this.formArit.setVisible(true);
            this.formArit.setLocationRelativeTo(null);
            Random r = new Random();
            int v1 = r.nextInt(10);
            int v2 = r.nextInt(10);
            num.setN1(v1);
            num.setN2(v2);
            num.setOperacion(4); // Indicando que la operación es una suma.
            num.setIntentos(0);
            String texto = "Calcule la división entre " + v1 + " y " + v2 + ". ";
            this.formArit.setTitle("DIVISIÓN");
            this.formArit.lblQuestion.setText(texto);
            this.formArit.txtRespuesta.setText("");
        }
        if(ae.getSource() == esc.jmiSalir){
            System.exit(0);
        }
        if(ae.getSource() == esc.jmiHistoria){
            this.form_histoty.jcb1.addItem("1. Ceiba");
            this.form_histoty.jcb1.addItem("2. Hormigo");
            this.form_histoty.jcb1.addItem("3. Chicozapote");
            this.form_histoty.jcb2.addItem("1. Lic. Carlos O. Zachrison");
            this.form_histoty.jcb2.addItem("2. General José María Orellana");
            this.form_histoty.jcb2.addItem("3. General Justo Rufino Barrios");
            this.form_histoty.jcb3.addItem("1. Francisco Pérez de Antón");
            this.form_histoty.jcb3.addItem("2. Francisco Pérez muñoz");
            this.form_histoty.jcb3.addItem("3. Francisco Roberto Sajché");
            this.form_histoty.jcb4.addItem("1. José María Reyna Barrios");
            this.form_histoty.jcb4.addItem("2. Justo Rufino Barrios");
            this.form_histoty.jcb4.addItem("3. Miguel García Granados");
            this.form_histoty.jcb5.addItem("1. Kaibil Balam");
            this.form_histoty.jcb5.addItem("2. Tecún Humán");
            this.form_histoty.jcb5.addItem("3. Atanasio Tzul");
            this.form_histoty.setTitle("HISTORIA DE GUATEMALA");
            this.form_histoty.setVisible(true);
            this.form_histoty.setLocationRelativeTo(esc);
        }
        if(ae.getSource() == esc.jmiAdivinaN){
            this.adivina.txtNumero.setText("");
            this.adivina.setTitle("ADIVINA EL NÚMERO");
            this.adivina.setVisible(true);
            this.adivina.setLocationRelativeTo(null);
        }
        if(ae.getSource() == esc.jmiTraductor){
            this.traslate.setTitle("TRADUCTOR");
            this.traslate.setVisible(true);
            this.traslate.setLocationRelativeTo(null);
        }
        if(ae.getSource() == esc.jmiUsuaios){
            user.setTitle("ADMINISTRACIÓN DE USUARIOS");
            user.setVisible(true);
            user.setLocationRelativeTo(esc);
        }
        if(ae.getSource() == esc.jmiReporte){
            
        }
    }

    @Override
    public void windowOpened(WindowEvent we) {
    }

    @Override
    public void windowClosing(WindowEvent we) {
        
    }

    @Override
    public void windowClosed(WindowEvent we) {
        
    }

    @Override
    public void windowIconified(WindowEvent we) {
        
    }

    @Override
    public void windowDeiconified(WindowEvent we) {
        
    }

    @Override
    public void windowActivated(WindowEvent we) {
        subiendoDeNivel();
    }

    @Override
    public void windowDeactivated(WindowEvent we) {
        subiendoDeNivel();
    }
    
    private void subiendoDeNivel(){
        esc.jmAdmon.setVisible(false);
        esc.jmAcciones.setVisible(false);
        esc.jmPrincipiante.setVisible(false);
        esc.jmIntermedio.setVisible(false);
        esc.jmAvanzado.setVisible(false);
        if(sesion.getFk_id_tipo_usuario() == 1) {
            esc.jmAdmon.setVisible(true);
        } else if(sesion.getFk_id_tipo_usuario() == 2){
            esc.jmAcciones.setVisible(true);
            esc.jmPrincipiante.setVisible(true);
        } else if(sesion.getFk_id_tipo_usuario() == 3){
            esc.jmAcciones.setVisible(true);
            esc.jmIntermedio.setVisible(true);
        } else if(sesion.getFk_id_tipo_usuario() == 4){
            esc.jmAcciones.setVisible(true);
            esc.jmAvanzado.setVisible(true);
        }
    }
    
}
