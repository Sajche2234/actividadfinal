package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.JOptionPane;
import modelo.Sesion;
import modelo.UsuarioDAO;
import vista.Frm_Adivina;

public class ControladorAdivina implements ActionListener {
    Frm_Adivina adivina = new Frm_Adivina();
    Random numAleatorio = new Random();
    int numero = numAleatorio.nextInt(15-1+1) + 1;
    Sesion sesion = new Sesion();
    UsuarioDAO users = new UsuarioDAO();

    public ControladorAdivina(Frm_Adivina adivina, Sesion sesion, UsuarioDAO users) {
        this.adivina = adivina;
        this.sesion = sesion;
        this.users = users;
        this.adivina.btnAdivinar.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource() == adivina.btnAdivinar){
            System.out.println(numero);
            try {
                if(adivina.txtNumero.getText().isEmpty()){
                    JOptionPane.showMessageDialog(null, "Es necesario que ingrese un valor en el campo de texto");
                } else {
                    int numeroUsuario = Integer.parseInt(adivina.txtNumero.getText());
                    if(numero == numeroUsuario){
                        //JOptionPane.showMessageDialog(null, "Correcto!");
                        // Darle el codigo de usuario, codigo de la accion, punteo y nivel o rol.
                        users.subirNivel(sesion.getId_usuario(), 6, 1, sesion.getFk_id_tipo_usuario());
                        JOptionPane.showMessageDialog(null, "Correcto! ", "RESUILTADO", JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(null, "Incorrecto!", "ERROR", JOptionPane.ERROR_MESSAGE);
                    }
                }
            } catch(NumberFormatException nf){
                JOptionPane.showMessageDialog(null, "Error: Solo se aceptan valores nuéricos. " + nf.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Error: " + e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
