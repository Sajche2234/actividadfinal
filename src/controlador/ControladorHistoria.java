package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import modelo.Sesion;
import modelo.UsuarioDAO;
import vista.Frm_Historia;

public class ControladorHistoria implements ActionListener {
Frm_Historia historia = new Frm_Historia();
Sesion sesion = new Sesion();
UsuarioDAO users = new UsuarioDAO();

    public ControladorHistoria(Frm_Historia historia, Sesion sesion, UsuarioDAO users) {
        this.historia = historia;
        this.sesion = sesion;
        this.users = users;
        historia.btnCalificar.addActionListener(this);
    }
    @Override
    public void actionPerformed(ActionEvent ae) {
        int sr1 = historia.jcb1.getSelectedIndex();
        int sr2 = historia.jcb2.getSelectedIndex();
        int sr3 = historia.jcb3.getSelectedIndex();
        int sr4 = historia.jcb4.getSelectedIndex();
        int sr5 = historia.jcb5.getSelectedIndex();
        int result = validar(sr1, sr2, sr3, sr4, sr5);
        if(result==0){
            //JOptionPane.showMessageDialog(null, "Felicidades, usted acaba de subir de nivel.");
            // Darle el codigo de usuario, codigo de la accion, punteo y nivel o rol.
            users.subirNivel(sesion.getId_usuario(), 5, 1, sesion.getFk_id_tipo_usuario());
            JOptionPane.showMessageDialog(null, "Correcto! " + sesion.getNombre_usuario() + " " + sesion.getApellido_usuario(), "RESUILTADO", JOptionPane.INFORMATION_MESSAGE);
            historia.dispose();
        } else {
            // Darle el codigo de usuario, codigo de la accion, punteo y nivel o rol.
            users.subirNivel(sesion.getId_usuario(), 5, 0, sesion.getFk_id_tipo_usuario());
        }
    }
    private int validar(int v1, int v2, int v3, int v4, int v5){
        int resultado=0;
        try {
            String r1 = (String) historia.jcb1.getSelectedItem();
            String r2 = (String) historia.jcb2.getSelectedItem();
            String r3 = (String) historia.jcb3.getSelectedItem();
            String r4 = (String) historia.jcb4.getSelectedItem();
            String r5 = (String) historia.jcb5.getSelectedItem();
            String msjErr1 = "La marimba no esta fabricada del arbol de " + r1;
            String msjErr2 = "El rostro del quetzal no corresponde a " + r2;
            String msjErr3 = r3 + " no es el autor del tema Luna de Xelajú";
            String msjErr4 = "La torre del reformador no se construyo en honor de " + r4;
            String msjErr5 = "El general quiché no fue " + r5;
            if(v1 != 1){
                resultado = 1;
                JOptionPane.showMessageDialog(null, "Error: " + msjErr1);
            }
            if(v2 != 1){
                resultado = 1;
                JOptionPane.showMessageDialog(null, "Error: " + msjErr2);
            }
            if(v3 != 1){
                resultado = 1;
                JOptionPane.showMessageDialog(null, "Error: " + msjErr3);
            }
            if(v4 != 1){
                resultado = 1;
                JOptionPane.showMessageDialog(null, "Error: " + msjErr4);
            }
            if(v5 != 1){
                resultado = 1;
                JOptionPane.showMessageDialog(null, "Error: " + msjErr5);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error: " + e.getMessage());
        }
        return resultado;
    }
}
