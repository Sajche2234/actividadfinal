package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.JOptionPane;
import modelo.Sesion;
import modelo.UsuarioDAO;
import vista.Frm_Traductor;

public class ControladorTraductor implements ActionListener {
    Frm_Traductor traductor = new Frm_Traductor();
    private String[] english;
    private String[] spanish;
    Random numAleatorio = new Random();
    int numero1, numero2, numero3;
    int r1=0, r2=0, r3=0, suma;
    Sesion sesion = new Sesion();
    UsuarioDAO users = new UsuarioDAO();
    String msj;

    public ControladorTraductor(Frm_Traductor traductor, Sesion sesion, UsuarioDAO users) {
        this.traductor = traductor;
        this.sesion = sesion;
        this.users = users;
        traductor.btnT1.addActionListener(this);
        traductor.btnT2.addActionListener(this);
        traductor.btnT3.addActionListener(this);
        numero1 = numAleatorio.nextInt(9) + 1;
        numero2 = numAleatorio.nextInt(9) + 1;
        numero3 = numAleatorio.nextInt(9) + 1;
        english=new String[10];
        spanish = new String[10];
        english[0]="HAVE";
        english[1]="WANT";
        english[2]="DAY";
        english[3]="WORK";
        english[4]="GET";
        english[5]="COME";
        english[6]="LOOK";
        english[7]="NOW";
        english[8]="GOOD";
        english[9]="TIME";
        spanish[0]="TENER";
        spanish[1]="QUERER";
        spanish[2]="DIA";
        spanish[3]="TRABAJAR";
        spanish[4]="OBTENER";
        spanish[5]="VENIR";
        spanish[6]="MIRAR";
        spanish[7]="AHORA";
        spanish[8]="BUENO";
        spanish[9]="TIEMPO";
        traductor.lbl1.setText(english[numero1]);
        traductor.lbl2.setText(english[numero2]);
        traductor.lbl3.setText(english[numero3]);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        msj = "FELICIDADES, usted ha ganado el juego: " + sesion.getNombre_usuario() + " " + sesion.getApellido_usuario() + ", con 9 puntos de 9...";
        if(ae.getSource() == traductor.btnT1){
            /*for(int f=0;f<english.length;f++) {
                System.out.println(english[f] + " - " + spanish[f]);         
            }*/            
            String dato1 = traductor.txt1.getText();
            if(dato1.equalsIgnoreCase(spanish[numero1])){
                r1=1;
                JOptionPane.showMessageDialog(null, "Correcto!");
            } else {
                r1=0;
                JOptionPane.showMessageDialog(null, "Incorrecto!");
            }
            suma = r1 + r2 + r3;
            if(suma==3){
                //JOptionPane.showMessageDialog(null, "Felicitaciones, usted ha ganado el JUEGO!");
                JOptionPane.showMessageDialog(null, msj, "RESUILTADO", JOptionPane.INFORMATION_MESSAGE);
                users.bitacora(sesion.getNombre_usuario(), "Ha finalizado exitosamente el juego.");
                //users.subirNivel(sesion.getId_usuario(), 7, 1, sesion.getFk_id_tipo_usuario());
            }
        }
        if(ae.getSource() == traductor.btnT2){
            String dato2 = traductor.txt2.getText();
            if(dato2.equalsIgnoreCase(spanish[numero2])){
                r2=1;
                JOptionPane.showMessageDialog(null, "Correcto!");
            } else {
                r2=0;
                JOptionPane.showMessageDialog(null, "Incorrecto!");
            }
            suma = r1 + r2 + r3;
            if(suma==3){
                JOptionPane.showMessageDialog(null, msj, "RESUILTADO", JOptionPane.INFORMATION_MESSAGE);
                users.bitacora(sesion.getNombre_usuario(), "Ha finalizado exitosamente el juego.");
            }
        }
        if(ae.getSource() == traductor.btnT3){
            String dato3 = traductor.txt3.getText();
            if(dato3.equalsIgnoreCase(spanish[numero3])){
                r3=1;
                JOptionPane.showMessageDialog(null, "Correcto!");
            } else {
                r3=0;
                JOptionPane.showMessageDialog(null, "Incorrecto!");
            }
            suma = r1 + r2 + r3;
            if(suma==3){
                JOptionPane.showMessageDialog(null, msj, "RESUILTADO", JOptionPane.INFORMATION_MESSAGE);
                users.bitacora(sesion.getNombre_usuario(), "Ha finalizado exitosamente el juego.");
            }
        }
    }
}
