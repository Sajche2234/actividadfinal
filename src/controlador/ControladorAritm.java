package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import modelo.NumerosVO;
import modelo.Sesion;
import modelo.UsuarioDAO;
import vista.Form_Aritmetica;

public class ControladorAritm implements ActionListener {
    Form_Aritmetica formArit = new Form_Aritmetica();
    NumerosVO num = new NumerosVO();
    Sesion sesion = new Sesion();
    UsuarioDAO users = new UsuarioDAO();

    public ControladorAritm(Form_Aritmetica formArit, NumerosVO num, Sesion sesion, UsuarioDAO users) {
        this.formArit = formArit;
        this.num = num;
        this.sesion = sesion;
        this.users = users;
        System.out.println("NUMEROS: " + num.getN1());
        formArit.btnCalcular.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        int ope = num.getOperacion();       
        if(ae.getSource() == formArit.btnCalcular){
            if(ope == 1) { suma(); }
            if(ope == 2) { resta(); }
            if(ope == 3) { multi(); }
            if(ope == 4) { division(); }
        }
    }
    private void suma(){
        try {
            int intent = num.getIntentos() + 1;
            int resultado = num.getN1() + num.getN2();
            int resultadoUs = Integer.parseInt(formArit.txtRespuesta.getText());
            if(resultado == resultadoUs) {
                // Darle el codigo de usuario, codigo de la accion, punteo y nivel o rol.
                users.subirNivel(sesion.getId_usuario(), 1, 1, sesion.getFk_id_tipo_usuario());
                JOptionPane.showMessageDialog(null, "Correcto! " + sesion.getNombre_usuario() + " " + sesion.getApellido_usuario(), "RESUILTADO", JOptionPane.INFORMATION_MESSAGE);
            } else {
                // Darle el codigo de usuario, codigo de la accion, punteo y nivel o rol.
                users.subirNivel(sesion.getId_usuario(), 1, 0, sesion.getFk_id_tipo_usuario());
                num.setIntentos(intent);
                if(intent > 2) {
                    JOptionPane.showMessageDialog(null, "Incorrecto!: " + resultado, "ERROR", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "Incorrecto!", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error! " + e.getMessage());
        }
    }
    private void resta(){
        try {
            int intent = num.getIntentos() + 1;
            int resultado = num.getN1() - num.getN2();
            int resultadoUs = Integer.parseInt(formArit.txtRespuesta.getText());
            if(resultado == resultadoUs) {
                // Darle el codigo de usuario, codigo de la accion, punteo y nivel o rol.
                users.subirNivel(sesion.getId_usuario(), 2, 1, sesion.getFk_id_tipo_usuario());
                JOptionPane.showMessageDialog(null, "Correcto! " + sesion.getNombre_usuario() + " " + sesion.getApellido_usuario(), "RESUILTADO", JOptionPane.INFORMATION_MESSAGE);
            } else {
                // Darle el codigo de usuario, codigo de la accion, punteo y nivel o rol.
                users.subirNivel(sesion.getId_usuario(), 2, 0, sesion.getFk_id_tipo_usuario());
                num.setIntentos(intent);
                if(intent > 2) {
                    JOptionPane.showMessageDialog(null, "Incorrecto!: " + resultado, "ERROR", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "Incorrecto!", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error! " + e.getMessage());
        }
    }
    private void multi(){
        try {
            int intent = num.getIntentos() + 1;
            int resultado = num.getN1() * num.getN2();
            int resultadoUs = Integer.parseInt(formArit.txtRespuesta.getText());
            if(resultado == resultadoUs) {
                // Darle el codigo de usuario, codigo de la accion, punteo y nivel o rol.
                users.subirNivel(sesion.getId_usuario(), 3, 1, sesion.getFk_id_tipo_usuario());
                JOptionPane.showMessageDialog(null, "Correcto! " + sesion.getNombre_usuario() + " " + sesion.getApellido_usuario(), "RESUILTADO", JOptionPane.INFORMATION_MESSAGE);
            } else {
                // Darle el codigo de usuario, codigo de la accion, punteo y nivel o rol.
                users.subirNivel(sesion.getId_usuario(), 3, 0, sesion.getFk_id_tipo_usuario());
                num.setIntentos(intent);
                if(intent > 2) {
                    JOptionPane.showMessageDialog(null, "Incorrecto!: " + resultado, "ERROR", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "Incorrecto!", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error! " + e.getMessage());
        }
    }
    private void division(){
        try {
            int intent = num.getIntentos() + 1;
            double resultado = num.getN1() / num.getN2();
            double resultadoUs = Double.parseDouble(formArit.txtRespuesta.getText());
            if(resultado == resultadoUs) {
                // Darle el codigo de usuario, codigo de la accion, punteo y nivel o rol.
                users.subirNivel(sesion.getId_usuario(), 4, 1, sesion.getFk_id_tipo_usuario());
                JOptionPane.showMessageDialog(null, "Correcto! " + sesion.getNombre_usuario() + " " + sesion.getApellido_usuario(), "RESUILTADO", JOptionPane.INFORMATION_MESSAGE);
            } else {
                // Darle el codigo de usuario, codigo de la accion, punteo y nivel o rol.
                users.subirNivel(sesion.getId_usuario(), 3, 0, sesion.getFk_id_tipo_usuario());
                num.setIntentos(intent);
                if(intent > 2) {
                    JOptionPane.showMessageDialog(null, "Incorrecto!: " + resultado, "ERROR", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "Incorrecto!", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error! " + e.getMessage());
        }
    }
}
