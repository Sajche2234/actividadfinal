package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.EstadoDAO;
import modelo.EstadoVO;
import modelo.TipoUsuarioDAO;
import modelo.TipoUsuarioVO;
import modelo.UsuarioDAO;
import modelo.UsuarioVO;
import vista.Frm_Usuario;

public class ControladorUsuario implements ActionListener, WindowListener, MouseListener, KeyListener {
    Frm_Usuario usuarios = new Frm_Usuario();
    UsuarioDAO udao = new UsuarioDAO();
    UsuarioVO uvo = new UsuarioVO();
    EstadoDAO edao = new EstadoDAO();
    EstadoVO evo = new EstadoVO();
    TipoUsuarioDAO tudao = new TipoUsuarioDAO();
    TipoUsuarioVO tuvo = new TipoUsuarioVO();

    public ControladorUsuario(Frm_Usuario usuarios, UsuarioDAO udao, UsuarioVO uvo, EstadoDAO edao, EstadoVO evo, TipoUsuarioDAO tudao, TipoUsuarioVO tuvo) {
        this.usuarios = usuarios;
        this.udao = udao;
        this.uvo = uvo;
        this.edao = edao;
        this.evo = evo;
        this.tudao = tudao;
        this.tuvo = tuvo;
        edao.mostrarEstados(usuarios.jcbEstado);
        tudao.mostrarRoles(usuarios.jcbTipoUsuario);
        usuarios.addWindowListener(this);
        usuarios.btnReiniciar.addActionListener(this);
        usuarios.btnNuevo.addActionListener(this);
        usuarios.tbUser.addMouseListener(this);
        usuarios.btnEditar.addActionListener(this);
        usuarios.btnEliminar.addActionListener(this);
        usuarios.txtFiltrar.addKeyListener(this);
    }
    
    private void mostarUsuarios(){
        DefaultTableModel m = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int filas, int columnas) {
                //return super.isCellEditable(i, i1); //To change body of generated methods, choose Tools | Templates.
                if(columnas==1){
                    return true;
                } else {
                    return false;
                }
            }
            
        };
        m.setColumnCount(0);
        m.addColumn("CÓDIGO");
        m.addColumn("NOMBRES");
        m.addColumn("APELLIDOS");
        m.addColumn("EDAD");
        m.addColumn("USUARIO");
        m.addColumn("ROL");
        m.addColumn("ESTADO");
        for (UsuarioVO uvo: udao.consultarTablaUsuario()) {
            m.addRow(new Object[]{
                uvo.getId_usuario(),
                uvo.getNombre_usuario(),
                uvo.getApellido_usuario(),
                uvo.getEdad_usuario(),
                uvo.getNick(),
                uvo.getRol(),
                uvo.getEstado()
            });
        }
        usuarios.tbUser.setModel(m);
    }
    // Funcion encargada de filtrar el contenido de la pabla por paso de parámetros.
    private void filtrarTabla(String parametro){
        DefaultTableModel m = new DefaultTableModel();
        m.setColumnCount(0);
        m.addColumn("CÓDIGO");
        m.addColumn("NOMBRES");
        m.addColumn("APELLIDOS");
        m.addColumn("EDAD");
        m.addColumn("USUARIO");
        m.addColumn("ROL");
        m.addColumn("ESTADO");
        for (UsuarioVO uvo: udao.consultarTablaUsuarioFiltro(parametro)) {
            m.addRow(new Object[]{
                uvo.getId_usuario(),
                uvo.getNombre_usuario(),
                uvo.getApellido_usuario(),
                uvo.getEdad_usuario(),
                uvo.getNick(),
                uvo.getRol(),
                uvo.getEstado()
            });
        }
        usuarios.tbUser.setModel(m);
    }
    // Función que inicializa todo.
    private void begin(){
        usuarios.txtID.setEditable(false);
        usuarios.btnEditar.setEnabled(false);
        usuarios.btnEliminar.setEnabled(false);
    }
    // Funcion encargada de limpiar los camos de texto y los combo box.
    private void clearFields(){
        usuarios.txtID.setText("");
        usuarios.txtNombres.setText("");
        usuarios.txtApellidos.setText("");
        usuarios.txtEdad.setText("");
        usuarios.txtUsuario.setText("");
        usuarios.txtPass.setText("");
        begin();
        mostarUsuarios();
    }
    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource() == usuarios.btnReiniciar){
            clearFields();
        }
        if(ae.getSource() == usuarios.btnNuevo){
            nuevoUsuario();
            /*System.out.println(usuarios.jcbEstado.getItemAt(usuarios.jcbEstado.getSelectedIndex()).getId_estado());
            System.out.println(usuarios.jcbTipoUsuario.getItemAt(usuarios.jcbTipoUsuario.getSelectedIndex()).getId_tipo_usuario());*/
        }
        if(ae.getSource() == usuarios.btnEditar){
            actualizarUsuario();
        }
        if(ae.getSource() == usuarios.btnEliminar){
            eliminarUsuario();
        }
    }
    private int validarCamposGenerales(){
        int result=0;
        if(usuarios.txtNombres.getText().isEmpty()){
            result=1;
            JOptionPane.showMessageDialog(null, "El campo NOMBRES es obligatorio.", "ERROR_MESSAGE", JOptionPane.ERROR_MESSAGE);
        }
        if(usuarios.txtApellidos.getText().isEmpty()){
            result=1;
            JOptionPane.showMessageDialog(null, "El campo APELLIDOS es obligatorio.", "ERROR_MESSAGE", JOptionPane.ERROR_MESSAGE);
        }
        if(usuarios.txtEdad.getText().isEmpty()){
            result=1;
            JOptionPane.showMessageDialog(null, "El campo EDAD es obligatorio.", "ERROR_MESSAGE", JOptionPane.ERROR_MESSAGE);
        }
        if(usuarios.txtUsuario.getText().isEmpty()){
            result=1;
            JOptionPane.showMessageDialog(null, "El campo USUARIO es obligatorio.", "ERROR_MESSAGE", JOptionPane.ERROR_MESSAGE);
        }
        return result;
    }
    private void nuevoUsuario(){
        int valida = this.validarCamposGenerales();
        try {
            if(valida==0){
                uvo.setNombre_usuario(usuarios.txtNombres.getText());
                uvo.setApellido_usuario(usuarios.txtApellidos.getText());
                uvo.setEdad_usuario(Integer.parseInt(usuarios.txtEdad.getText()));
                uvo.setNick(usuarios.txtUsuario.getText());
                uvo.setFk_id_estado(usuarios.jcbEstado.getItemAt(usuarios.jcbEstado.getSelectedIndex()).getId_estado());
                uvo.setFk_id_tipo_usuario(usuarios.jcbTipoUsuario.getItemAt(usuarios.jcbTipoUsuario.getSelectedIndex()).getId_tipo_usuario());
                if(usuarios.txtPass.getText().isEmpty()){
                    // Llamar funcion para poder crear el usuario sin contrasñea.
                    udao.insert(uvo);
                    clearFields();
                    JOptionPane.showMessageDialog(null, "Se ha creado el registro de usuario exitosamente.", "RESULTADO", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    // Crear el usuario con todos los datos.
                    uvo.setPassword(usuarios.txtPass.getText());
                    udao.insertWithPass(uvo);
                    clearFields();
                    JOptionPane.showMessageDialog(null, "Se ha creado el registro de usuario exitosamente.", "RESULTADO", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        } catch (NumberFormatException ex){
            JOptionPane.showMessageDialog(null, "Error: Sólo se aceptan números " + ex.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error en la sección de insertar: " + e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }
    private void actualizarUsuario(){
        int valida = this.validarCamposGenerales();
        try {
            int id = Integer.parseInt(usuarios.txtID.getText());
            if(usuarios.txtID.getText().isEmpty()){
                valida = 1;
                JOptionPane.showMessageDialog(null, "El campo CODIGO es obligatorio", "ERROR_MESSAGE", JOptionPane.ERROR_MESSAGE);
            }
            if(valida==0){
                uvo.setId_usuario(id);
                uvo.setNombre_usuario(usuarios.txtNombres.getText());
                uvo.setApellido_usuario(usuarios.txtApellidos.getText());
                uvo.setEdad_usuario(Integer.parseInt(usuarios.txtEdad.getText()));
                uvo.setNick(usuarios.txtUsuario.getText());
                uvo.setFk_id_estado(usuarios.jcbEstado.getItemAt(usuarios.jcbEstado.getSelectedIndex()).getId_estado());
                uvo.setFk_id_tipo_usuario(usuarios.jcbTipoUsuario.getItemAt(usuarios.jcbTipoUsuario.getSelectedIndex()).getId_tipo_usuario());
                if(usuarios.txtPass.getText().isEmpty()){
                    // Llamar funcion para poder actuzliar el usuario sin contrasñea.
                    udao.actualizar(uvo);
                    clearFields();
                    JOptionPane.showMessageDialog(null, "Se ha actualizado el registro de usuario exitosamente.", "RESULTADO", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    // Crear el usuario con todos los datos.
                    uvo.setPassword(usuarios.txtPass.getText());
                    udao.actualizarWithPass(uvo);
                    clearFields();
                    JOptionPane.showMessageDialog(null, "Se ha actualizado el registro de usuario exitosamente.", "ERROR", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        } catch (NumberFormatException ex){
            JOptionPane.showMessageDialog(null, "Error: Sólo se aceptan números " + ex.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
             JOptionPane.showMessageDialog(null, "Error en la sección de insertar: " + e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }
    private void eliminarUsuario(){
        int valida = this.validarCamposGenerales();
        try {
            if(usuarios.txtID.getText().isEmpty()){
                valida = 1;
                JOptionPane.showMessageDialog(null, "El campo CODIGO es obligatorio", "ERROR", JOptionPane.ERROR_MESSAGE);
            }
            if(valida==0){
                udao.eliminar(uvo);
                clearFields();
                JOptionPane.showMessageDialog(null, "Se ha eliminado el registro de usuario exitosamente.", "RESUILTADO", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (NumberFormatException ex){
            JOptionPane.showMessageDialog(null, "Error: Sólo se aceptan números " + ex.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
             JOptionPane.showMessageDialog(null, "Error en la sección de insertar: " + e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }
    private void cargarDatos(){
        try {
            int seleccion = usuarios.tbUser.getSelectedRow();
            uvo.setId_usuario((int) usuarios.tbUser.getValueAt(seleccion, 0));
            for(UsuarioVO usvo : udao.getUsuario(uvo)){
                usuarios.btnEditar.setEnabled(true);
                usuarios.btnEliminar.setEnabled(true);
                usuarios.btnNuevo.setEnabled(false);
                usuarios.txtID.setEditable(true);
                usuarios.txtID.setText(String.valueOf(usvo.getId_usuario()));
                usuarios.txtNombres.setText(usvo.getNombre_usuario());
                usuarios.txtApellidos.setText(usvo.getApellido_usuario());
                usuarios.txtEdad.setText(String.valueOf(usvo.getEdad_usuario()));
                usuarios.txtUsuario.setText(usvo.getNick());
                //usuarios.jcbEstado.setSelectedIndex(usvo.getFk_id_estado());
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error en la sección de cargar datos: " + e.getMessage(), "ERROR_MESSAGE", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void windowOpened(WindowEvent we) {
        this.mostarUsuarios();
        clearFields();
    }

    @Override
    public void windowClosing(WindowEvent we) {
        
    }

    @Override
    public void windowClosed(WindowEvent we) {
        
    }

    @Override
    public void windowIconified(WindowEvent we) {
        
    }

    @Override
    public void windowDeiconified(WindowEvent we) {
        
    }

    @Override
    public void windowActivated(WindowEvent we) {
        this.mostarUsuarios();
    }

    @Override
    public void windowDeactivated(WindowEvent we) {
        
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        cargarDatos();
    }

    @Override
    public void mousePressed(MouseEvent me) {
        
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        
    }

    @Override
    public void mouseExited(MouseEvent me) {
        
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        this.filtrarTabla(usuarios.txtFiltrar.getText());
    }
    
}
