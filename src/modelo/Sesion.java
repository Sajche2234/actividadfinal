package modelo;
public class Sesion {
    private int id_usuario;
    private String nombre_usuario;
    private String apellido_usuario;
    private int fk_id_tipo_usuario;

    public Sesion() {
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getNombre_usuario() {
        return nombre_usuario;
    }

    public void setNombre_usuario(String nombre_usuario) {
        this.nombre_usuario = nombre_usuario;
    }

    public String getApellido_usuario() {
        return apellido_usuario;
    }

    public void setApellido_usuario(String apellido_usuario) {
        this.apellido_usuario = apellido_usuario;
    }

    public int getFk_id_tipo_usuario() {
        return fk_id_tipo_usuario;
    }

    public void setFk_id_tipo_usuario(int fk_id_tipo_usuario) {
        this.fk_id_tipo_usuario = fk_id_tipo_usuario;
    }
}
