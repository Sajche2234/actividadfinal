/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import javax.swing.JComboBox;

/**
 *
 * @author Roberto
 */
public interface ConsultasEstadosDAO {
    public ArrayList<EstadoVO> listadoEstados();
    public void mostrarEstados(JComboBox<EstadoVO> comboEstado);
}
