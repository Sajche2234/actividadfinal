package modelo;

import java.util.ArrayList;
import javax.swing.JComboBox;
public interface ConsultasTiposDAO {
    public ArrayList<TipoUsuarioVO> listadoTipos();
    public void mostrarRoles(JComboBox<TipoUsuarioVO> comboRoles);
}
