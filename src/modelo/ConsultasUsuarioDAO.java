package modelo;

import java.util.ArrayList;

public interface ConsultasUsuarioDAO {
    public void insertWithPass(UsuarioVO usr);
    public void insert(UsuarioVO usr);
    public void actualizar(UsuarioVO usr);
    public void actualizarWithPass(UsuarioVO usr);
    public void eliminar(UsuarioVO usr);
    public int usuario(UsuarioVO usr);
    public ArrayList<UsuarioVO> consultarTablaUsuario();
    public ArrayList<UsuarioVO> getUsuario(UsuarioVO usr);
    public ArrayList<UsuarioVO> consultarTablaUsuarioFiltro(String parametro);
    public void subirNivel(int usuario, int accion, int punteo, int nivel);
}
