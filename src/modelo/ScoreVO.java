package modelo;
public class ScoreVO {
    private int id_score;
    private int punteo_score;
    private int fk_id_usuario;

    public ScoreVO() {
    }

    public int getId_score() {
        return id_score;
    }

    public void setId_score(int id_score) {
        this.id_score = id_score;
    }

    public int getPunteo_score() {
        return punteo_score;
    }

    public void setPunteo_score(int punteo_score) {
        this.punteo_score = punteo_score;
    }

    public int getFk_id_usuario() {
        return fk_id_usuario;
    }

    public void setFk_id_usuario(int fk_id_usuario) {
        this.fk_id_usuario = fk_id_usuario;
    }
    
}
