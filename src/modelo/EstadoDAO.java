package modelo;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.swing.JComboBox;

public class EstadoDAO implements ConsultasEstadosDAO {

    @Override
    public ArrayList<EstadoVO> listadoEstados() {
        Conector c = new Conector();
        ArrayList<EstadoVO> info = new ArrayList<>();
        try {
            c.conectar();
            String consulta="SELECT id_estado, nombre_estado FROM tbl_estado ORDER BY nombre_estado ASC";
            ResultSet rs = c.consulta_datos(consulta);
            while(rs.next()){
                EstadoVO evo = new EstadoVO();
                evo.setId_estado(rs.getInt(1));
                evo.setNombre_estado(rs.getString(2));
                // Como le habo para presentar el texto de autor en la tabla
                info.add(evo);
            }
            c.desconectar();
        } catch (Exception e) {
            System.err.println("Error en consulta simple: " + e.getMessage());
        }
        return info;
    }

    @Override
    public void mostrarEstados(JComboBox<EstadoVO> comboEstado) {
        Conector c = new Conector();
        try {
            c.conectar();
            String consulta="SELECT id_estado, nombre_estado, descripcion_estado FROM tbl_estado ORDER BY nombre_estado ASC";
            ResultSet rs = c.consulta_datos(consulta);
            while(rs.next()){
                comboEstado.addItem(
                        new EstadoVO (
                                rs.getInt("id_estado"),
                                rs.getString("nombre_estado"),
                                rs.getString("descripcion_estado")
                        )
                );
            }
        } catch (Exception e) {
            System.out.println("Error al mostrar el combo de estados: " + e.getMessage());
        }
    }
    
}
