package modelo;

import controlador.ControladorLogin;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class UsuarioDAO implements ConsultasUsuarioDAO {
    Sesion sesion = new Sesion();

    @Override
    public void insert(UsuarioVO usr) {
        Conector c = new Conector();
        try {
            c.conectar();
            String consulta = "INSERT INTO tbl_usuario (id_usuario, nombre_usuario, apellido_usuario, edad_usuario, nick, fk_id_estado, fk_id_tipo_usuario) VALUES (NULL, '"+usr.getNombre_usuario()+"', '"+usr.getApellido_usuario()+"', '"+usr.getEdad_usuario()+"', '"+usr.getNick()+"', '"+usr.getFk_id_estado()+"', '"+usr.getFk_id_tipo_usuario()+"')";
            c.consulta_multiple(consulta);
            bitacora("usuario", "Se ha creado al usuario: " + usr.getNombre_usuario() + " " + usr.getApellido_usuario());
        } catch (Exception e) {
            System.err.println("Error al crear: " + e.getMessage());
        }
    }

    @Override
    public void actualizar(UsuarioVO usr) {
        Conector c = new Conector();
        try {
            c.conectar();            
            String consulta = "UPDATE tbl_usuario SET nombre_usuario = '"+usr.getNombre_usuario()+"', apellido_usuario = '"+usr.getApellido_usuario()+"', edad_usuario='"+usr.getEdad_usuario()+"', nick='"+usr.getNick()+"', fk_id_estado = '"+usr.getFk_id_estado()+"', fk_id_tipo_usuario = '"+usr.getFk_id_tipo_usuario()+"' WHERE id_usuario = '"+usr.getId_usuario()+"'";
            c.consulta_multiple(consulta);
            bitacora("usuario", "Se ha modificado los datos del usuario: " + usr.getNombre_usuario() + " " + usr.getApellido_usuario());
        } catch (Exception e) {
            System.err.println("Error al actualizar: " + e.getMessage());
        }
    }
    
    @Override
    public void insertWithPass(UsuarioVO usr) {
        Conector c = new Conector();
        try {
            c.conectar();
            String consulta = "INSERT INTO tbl_usuario (id_usuario, nombre_usuario, apellido_usuario, edad_usuario, nick, password, fk_id_estado, fk_id_tipo_usuario) VALUES (NULL, '"+usr.getNombre_usuario()+"', '"+usr.getApellido_usuario()+"', '"+usr.getEdad_usuario()+"', '"+usr.getNick()+"', '"+usr.getPassword()+"', '"+usr.getFk_id_estado()+"', '"+usr.getFk_id_tipo_usuario()+"')";
            c.consulta_multiple(consulta);
            bitacora("usuario", "Se ha creado el usuario: " + usr.getNombre_usuario() + " " + usr.getApellido_usuario());
        } catch (Exception e) {
            System.err.println("Error al crear: " + e.getMessage());
        }
    }

    @Override
    public void actualizarWithPass(UsuarioVO usr) {
        Conector c = new Conector();
        try {
            c.conectar();            
            String consulta = "UPDATE tbl_usuario SET nombre_usuario = '"+usr.getNombre_usuario()+"', apellido_usuario = '"+usr.getApellido_usuario()+"', edad_usuario='"+usr.getEdad_usuario()+"', nick='"+usr.getNick()+"', password = '"+usr.getPassword()+"', fk_id_estado = '"+usr.getFk_id_estado()+"', fk_id_tipo_usuario = '"+usr.getFk_id_tipo_usuario()+"' WHERE id_usuario = '"+usr.getId_usuario()+"'";
            c.consulta_multiple(consulta);
            bitacora("usuario", "Se ha modificado los datos del usuario: " + usr.getNombre_usuario() + " " + usr.getApellido_usuario());
        } catch (Exception e) {
            System.err.println("Error al actualizar: " + e.getMessage());
        }
    }

    @Override
    public void eliminar(UsuarioVO usr) {
        Conector c = new Conector();
        try {
            c.conectar();            
            String consulta = "UPDATE tbl_usuario SET fk_id_estado = 2 WHERE id_usuario = '"+usr.getId_usuario()+"'";
            c.consulta_multiple(consulta);
            bitacora("usuario", "Se ha eliminado al usuario, o mas bien, ha cambiado de estado a INACTIVO.");
        } catch (Exception e) {
            System.err.println("Error al actualizar: " + e.getMessage());
        }
    }

    @Override
    public int usuario(UsuarioVO usr) {
        Conector c = new Conector();
        int codigo=0;
        int existe=0;
        try {
            c.conectar();
            String consulta = "SELECT * FROM tbl_usuario WHERE nick='"+usr.getNick()+"' AND password = '"+usr.getPassword()+"' LIMIT 0,1";
            Statement st = c.con.createStatement();
            ResultSet rs = st.executeQuery(consulta);
            if(rs.next()){
                /*System.out.println("ID: " + rs.getInt("id_usuario"));
                System.out.println("NOMBRE: " + rs.getString("nombre_usuario"));
                System.out.println("ESTADO: " + rs.getInt("fk_id_estado"));
                System.out.println("ROL: " + rs.getInt("fk_id_tipo_usuario"));*/
                sesion.setId_usuario(rs.getInt("id_usuario"));
                sesion.setNombre_usuario(rs.getString("nombre_usuario"));
                sesion.setApellido_usuario(rs.getString("apellido_usuario"));
                sesion.setFk_id_tipo_usuario(rs.getInt("fk_id_tipo_usuario"));
                existe=1; codigo = rs.getInt("id_usuario");
            }
        } catch (Exception e) {
            System.err.println("Error al consultar el usuario: " + e.getMessage());
        }
        return codigo;
    }

    @Override
    public ArrayList<UsuarioVO> consultarTablaUsuario() {
        Conector c = new Conector();
        ArrayList<UsuarioVO> info = new ArrayList<>();
        try {
            c.conectar();
            String consulta="SELECT A.id_usuario, A.nombre_usuario, A.apellido_usuario, A.edad_usuario, A.nick, B.nombre_tipo_usuario, C.nombre_estado FROM tbl_usuario AS A INNER JOIN tbl_tipo_usuario AS B ON B.id_tipo_usuario = A.fk_id_tipo_usuario INNER JOIN tbl_estado AS C ON C.id_estado=A.fk_id_estado ORDER BY A.id_usuario DESC";
            ResultSet rs = c.consulta_datos(consulta);
            while(rs.next()){
                UsuarioVO uvo = new UsuarioVO();
                uvo.setId_usuario(rs.getInt(1));
                uvo.setNombre_usuario(rs.getString(2));
                uvo.setApellido_usuario(rs.getString(3));
                uvo.setEdad_usuario(rs.getInt(4));
                uvo.setNick(rs.getString(5));
                uvo.setRol(rs.getString(6));
                uvo.setEstado(rs.getString(7));
                // Como le habo para presentar el texto de autor en la tabla
                info.add(uvo);
            }
            c.desconectar();
        } catch (Exception e) {
            System.err.println("Error en consulta simple: " + e.getMessage());
        }
        return info;
    }
    public void bitacora(String usuario, String accion){
        Conector c = new Conector();
        try {
            InetAddress address = null;
            try {
                address = InetAddress.getLocalHost();
            } catch (UnknownHostException ex) {
                Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
            c.conectar();
            String consulta = "INSERT INTO bitacora (codigo, ip, usuario, created_at, accion) VALUES (NULL, '"+address.getHostAddress()+"', '"+usuario+"', NOW(), '"+accion+"')";
            c.consulta_multiple(consulta);
        } catch (Exception e) {
            System.err.println("Error en la función de bitacora: " + e.getMessage());
        }
    }        

    @Override
    public ArrayList<UsuarioVO> getUsuario(UsuarioVO usr) {
        Conector c = new Conector();
        ArrayList<UsuarioVO> info = new ArrayList<>();
        try {
            c.conectar();
            String consulta="SELECT id_usuario, nombre_usuario, apellido_usuario, edad_usuario, nick, fk_id_estado, fk_id_tipo_usuario FROM tbl_usuario WHERE id_usuario='"+usr.getId_usuario()+"'";
            ResultSet rs = c.consulta_datos(consulta);
            while(rs.next()){
                UsuarioVO uvo = new UsuarioVO();
                uvo.setId_usuario(rs.getInt(1));
                uvo.setNombre_usuario(rs.getString(2));
                uvo.setApellido_usuario(rs.getString(3));
                uvo.setEdad_usuario(rs.getInt(4));
                uvo.setNick(rs.getString(5));
                uvo.setFk_id_estado(rs.getInt(6));
                uvo.setFk_id_tipo_usuario(rs.getInt(7));
                info.add(uvo);
            }
            c.desconectar();
        } catch (Exception e) {
            System.err.println("Error en consulta simple: " + e.getMessage());
        }
        return info;
    }

    @Override
    public ArrayList<UsuarioVO> consultarTablaUsuarioFiltro(String parametro) {
        Conector c = new Conector();
        ArrayList<UsuarioVO> info = new ArrayList<>();
        try {
            c.conectar();
            String consulta="SELECT A.id_usuario, A.nombre_usuario, A.apellido_usuario, A.edad_usuario, A.nick, B.nombre_tipo_usuario, C.nombre_estado FROM tbl_usuario AS A INNER JOIN tbl_tipo_usuario AS B ON B.id_tipo_usuario = A.fk_id_tipo_usuario INNER JOIN tbl_estado AS C ON C.id_estado=A.fk_id_estado WHERE A.nombre_usuario LIKE '%"+parametro+"%' OR A.apellido_usuario LIKE '%"+parametro+"%' OR A.nick LIKE '%"+parametro+"%' OR B.nombre_tipo_usuario LIKE '%"+parametro+"%' ORDER BY A.id_usuario DESC";
            ResultSet rs = c.consulta_datos(consulta);
            while(rs.next()){
                UsuarioVO uvo = new UsuarioVO();
                uvo.setId_usuario(rs.getInt(1));
                uvo.setNombre_usuario(rs.getString(2));
                uvo.setApellido_usuario(rs.getString(3));
                uvo.setEdad_usuario(rs.getInt(4));
                uvo.setNick(rs.getString(5));
                uvo.setRol(rs.getString(6));
                uvo.setEstado(rs.getString(7));
                // Como le habo para presentar el texto de autor en la tabla
                info.add(uvo);
            }
            c.desconectar();
        } catch (Exception e) {
            System.err.println("Error en consulta simple con paso de parámetros: " + e.getMessage());
        }
        return info;
    }

    @Override
    public void subirNivel(int usuario, int accion, int punteo, int nivel) {
        Conector c = new Conector();
        String consulta = "SELECT COUNT(*) AS existe FROM tbl_score WHERE fk_id_usuario = '"+usuario+"' AND fk_accion = '"+accion+"'";
        String query="", queryNivel="";
        int punteoTotal=0;
        try {
            c.conectar();
            ResultSet rs = c.consulta_datos(consulta);
            while(rs.next()){
                int existe = rs.getInt(1);
                if(existe==0){
                    query="INSERT INTO tbl_score (id_score, punteo_score, fk_id_usuario, fk_nivel_usuario, fk_accion, created_at) VALUES (NULL, "+punteo+", "+usuario+", "+nivel+", "+accion+", NOW())";
                    c.consulta_multiple(query);
                    // Comprobando si sube de nivel
                    queryNivel = "SELECT SUM(punteo_score) AS total FROM tbl_score WHERE fk_id_usuario="+usuario+" ANd fk_nivel_usuario="+nivel+"";
                    ResultSet rs2 = c.consulta_datos(queryNivel);
                    while(rs2.next()){
                        punteoTotal = rs2.getInt(1);
                        // Verifica el nivel que le corresponde al usuario.
                        if((nivel==2) && (punteoTotal==4)){
                            JOptionPane.showMessageDialog(null, "Felicidades, ha subido de nivel (INTERMEDIO)", "WIIIII!!!", JOptionPane.INFORMATION_MESSAGE);
                            query="UPDATE tbl_usuario SET fk_id_tipo_usuario = 3 WHERE id_usuario = " + usuario;
                            c.consulta_multiple(query);
                            this.bitacora(sesion.getNombre_usuario(), "Ha pasado al nivel INTERMEDIO");
                            sesion.setFk_id_tipo_usuario(3);
                        }
                        if((nivel==3) && (punteoTotal==1)){
                            JOptionPane.showMessageDialog(null, "Felicidades, ha subido de nivel (AVANZADO)", "WIIIII!!!", JOptionPane.INFORMATION_MESSAGE);
                            query="UPDATE tbl_usuario SET fk_id_tipo_usuario = 4 WHERE id_usuario = " + usuario;
                            c.consulta_multiple(query);
                            this.bitacora(sesion.getNombre_usuario(), "Ha pasado al nivel AVANZADO");
                            sesion.setFk_id_tipo_usuario(4);
                        }
                        if((nivel==4) && (punteoTotal==4)){
                            JOptionPane.showMessageDialog(null, "Felicidades, usted ha ganado el juego: " + sesion.getNombre_usuario() + " " + sesion.getApellido_usuario(), "WIIIII!!! 7 puntos de 7", JOptionPane.INFORMATION_MESSAGE);
                        }
                        
                    }
                } else {
                    query="UPDATE tbl_score SET punteo_score = "+punteo+", updated_at = NOW() WHERE fk_id_usuario = '"+usuario+"' AND fk_accion = '"+accion+"'";
                    c.consulta_multiple(query);
                    queryNivel = "SELECT SUM(punteo_score) AS total FROM tbl_score WHERE fk_id_usuario="+usuario+" ANd fk_nivel_usuario="+nivel+"";
                    ResultSet rs2 = c.consulta_datos(queryNivel);
                    while(rs2.next()){
                        punteoTotal = rs2.getInt(1);
                        // Verifica el nivel que le corresponde al usuario.
                        if((nivel==2) && (punteoTotal==4)){
                            JOptionPane.showMessageDialog(null, "Felicidades, ha subido de nivel (INTERMEDIO)", "WIIIII!!!", JOptionPane.INFORMATION_MESSAGE);
                            query="UPDATE tbl_usuario SET fk_id_tipo_usuario = 3 WHERE id_usuario = " + usuario;
                            c.consulta_multiple(query);
                            sesion.setFk_id_tipo_usuario(3);
                        }
                        if((nivel==3) && (punteoTotal==1)){
                            JOptionPane.showMessageDialog(null, "Felicidades, ha subido de nivel (AVANZADO)", "WIIIII!!!", JOptionPane.INFORMATION_MESSAGE);
                            query="UPDATE tbl_usuario SET fk_id_tipo_usuario = 4 WHERE id_usuario = " + usuario;
                            c.consulta_multiple(query);
                            this.bitacora(sesion.getNombre_usuario(), "Ha pasado al nivel INTERMEDIO");
                            sesion.setFk_id_tipo_usuario(4);
                        }
                        if((nivel==4) && (punteoTotal==4)){
                            this.bitacora(sesion.getNombre_usuario(), "Ha pasado al nivel AVANZADO");
                            JOptionPane.showMessageDialog(null, "Felicidades, usted ha ganado el juego: " + sesion.getNombre_usuario() + " " + sesion.getApellido_usuario(), "WIIIII!!! 7 puntos de 7", JOptionPane.INFORMATION_MESSAGE);
                        }
                        
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("Error en consulta puntaje: " + e.getMessage());
        }
    }
}
