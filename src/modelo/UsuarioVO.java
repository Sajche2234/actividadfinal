package modelo;
public class UsuarioVO {
    private int id_usuario;
    private String nombre_usuario;
    private String apellido_usuario;
    private int edad_usuario;
    private String nick;
    private String password;
    private int fk_id_estado;
    private int fk_id_tipo_usuario;
    private String estado, rol;

    public UsuarioVO() {
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getNombre_usuario() {
        return nombre_usuario;
    }

    public void setNombre_usuario(String nombre_usuario) {
        this.nombre_usuario = nombre_usuario;
    }

    public String getApellido_usuario() {
        return apellido_usuario;
    }

    public void setApellido_usuario(String apellido_usuario) {
        this.apellido_usuario = apellido_usuario;
    }

    public int getEdad_usuario() {
        return edad_usuario;
    }

    public void setEdad_usuario(int edad_usuario) {
        this.edad_usuario = edad_usuario;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getFk_id_estado() {
        return fk_id_estado;
    }

    public void setFk_id_estado(int fk_id_estado) {
        this.fk_id_estado = fk_id_estado;
    }

    public int getFk_id_tipo_usuario() {
        return fk_id_tipo_usuario;
    }

    public void setFk_id_tipo_usuario(int fk_id_tipo_usuario) {
        this.fk_id_tipo_usuario = fk_id_tipo_usuario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
}
