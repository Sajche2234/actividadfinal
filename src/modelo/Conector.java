package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Conector {
    private String driver="com.mysql.jdbc.Driver"; // Driver de conexión
    private String servidor = "localhost"; // Dirección del servidor.
    private String usuario = "root"; // Usuario de la base de datos.
    private String password = ""; // Contraseña
    private String bd = "actividad_final"; // Nombre de la base de datos.
    private String cadena;
    Connection con;
    Statement st;
    public void conectar(){
        this.cadena = "jdbc:mysql://"+this.servidor+"/"+this.bd;
        try {
            Class.forName(this.driver).newInstance(); // La Class, y ForName, la buscan y la encuentran. y lo hace parte del proyecto.
            this.con = DriverManager.getConnection(this.cadena, this.usuario, this.password);
        } catch (Exception e) {
            System.err.println("Error Conexion: " + e.getMessage());
        }
    }
    public void desconectar(){
        try {
            this.con.close();
        } catch (Exception e) {
            System.err.println("Error cierre de conexión: " + e.getMessage());
        }
    }
    public int consulta_multiple(String consulta){
        int resultado;
        try {
            this.conectar();
            this.st = this.con.createStatement();
            resultado = this.st.executeUpdate(consulta);
        } catch (Exception e) {
            System.out.println("Error al ejecutar la consulta: " + e.getMessage());
            resultado=0;
        } finally {
            this.desconectar();
        }
        return resultado;
    }
    public ResultSet consulta_datos(String consulta){
        try {
            this.conectar();
            ResultSet resultado = null;
            this.st = this.con.createStatement();
            resultado = st.executeQuery(consulta);
            return resultado;
        } catch (Exception e) {
            System.err.println("Error en la funcion consulta_datos: " + e.getMessage());
        } /*finally {
            this.desconectar();
        }*/
        return null;
    }
}
