package modelo;

import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

public class TipoUsuarioDAO implements ConsultasTiposDAO {

    @Override
    public ArrayList<TipoUsuarioVO> listadoTipos() {
        Conector c = new Conector();
        ArrayList<TipoUsuarioVO> info = new ArrayList<>();
        try {
            c.conectar();
            String consulta="SELECT id_tipo_usuario, nombre_tipo_usuario FROM tbl_tipo_usuario ORDER BY id_tipo_usuario ASC";
            ResultSet rs = c.consulta_datos(consulta);
            while(rs.next()){
                TipoUsuarioVO tuvo = new TipoUsuarioVO();
                tuvo.setId_tipo_usuario(rs.getInt(1));
                tuvo.setNombre_tipo_usuario(rs.getString(2));
                // Como le habo para presentar el texto de tipo de usuario en el objeto
                info.add(tuvo);
            }
            c.desconectar();
        } catch (Exception e) {
            System.err.println("Error en consulta simple: " + e.getMessage());
        }
        return info;
    }

    @Override
    public void mostrarRoles(JComboBox<TipoUsuarioVO> comboRoles) {
        Conector c = new Conector();
        try {
            c.conectar();
            String consulta="SELECT id_tipo_usuario, nombre_tipo_usuario, descripcion_tipo_usuario, puntaje FROM tbl_tipo_usuario ORDER BY id_tipo_usuario ASC";
            ResultSet rs = c.consulta_datos(consulta);
            while(rs.next()){
                comboRoles.addItem(
                        new TipoUsuarioVO(
                                rs.getInt("id_tipo_usuario"),
                                rs.getString("nombre_tipo_usuario"),
                                rs.getString("descripcion_tipo_usuario")
                        )
                );
            }
        } catch (Exception e) {
            System.out.println("Error al mostrar el combo de Roles: " + e.getMessage());
        }
    }
    
}
