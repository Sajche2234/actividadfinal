/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actividadfinal;

import controlador.ControladorAdivina;
import controlador.ControladorAritm;
import controlador.ControladorEscritorio;
import controlador.ControladorHistoria;
import controlador.ControladorLogin;
import controlador.ControladorTraductor;
import controlador.ControladorUsuario;
import modelo.EstadoDAO;
import modelo.EstadoVO;
import modelo.NumerosVO;
import modelo.Sesion;
import modelo.TipoUsuarioDAO;
import modelo.TipoUsuarioVO;
import modelo.UsuarioDAO;
import modelo.UsuarioVO;
import vista.Form_Aritmetica;
import vista.Frm_Adivina;
import vista.Frm_Escritorio;
import vista.Frm_Historia;
import vista.Frm_Login;
import vista.Frm_Traductor;
import vista.Frm_Usuario;

/**
 *
 * @author Roberto
 */
public class ActividadFinal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Frm_Login login = new Frm_Login();
        Frm_Escritorio formEsc = new Frm_Escritorio();
        Form_Aritmetica formArit = new Form_Aritmetica();
        Frm_Historia form_histoty = new Frm_Historia();
        Frm_Adivina adivina = new Frm_Adivina();
        Frm_Traductor traslate = new Frm_Traductor();
        Frm_Usuario frmuser = new Frm_Usuario();
        // Instancias los objetos.
        UsuarioVO uvo = new UsuarioVO();
        UsuarioDAO udao = new UsuarioDAO();
        NumerosVO num = new NumerosVO();
        Sesion sesion = new Sesion();
        EstadoDAO edao = new EstadoDAO();
        EstadoVO evo = new EstadoVO();
        TipoUsuarioDAO tudao = new TipoUsuarioDAO();
        TipoUsuarioVO tuvo = new TipoUsuarioVO();
        // Instanciar los controladores
        ControladorLogin clog = new ControladorLogin(login, uvo, udao, formEsc, formArit, num, sesion);
        ControladorEscritorio esc = new ControladorEscritorio(formEsc, login, uvo, udao, formArit, num, sesion, form_histoty, adivina, traslate, frmuser);
        ControladorAritm crit = new ControladorAritm(formArit, num, sesion, udao);
        ControladorHistoria chis = new ControladorHistoria(form_histoty, sesion, udao);
        ControladorAdivina cadiv = new ControladorAdivina(adivina, sesion, udao);
        ControladorTraductor ctrad = new ControladorTraductor(traslate, sesion, udao);
        ControladorUsuario cuser = new ControladorUsuario(frmuser, udao, uvo, edao, evo, tudao, tuvo);
        // Mostrar el formulario en pantalla
        login.setVisible(true);
        login.setLocationRelativeTo(null);
    }
    
}
