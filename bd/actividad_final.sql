-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-06-2021 a las 06:13:15
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `actividad_final`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora`
--

CREATE TABLE `bitacora` (
  `codigo` int(11) NOT NULL,
  `ip` varchar(35) COLLATE latin1_spanish_ci NOT NULL,
  `usuario` varchar(35) COLLATE latin1_spanish_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `accion` varchar(255) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `bitacora`
--

INSERT INTO `bitacora` (`codigo`, `ip`, `usuario`, `created_at`, `accion`) VALUES
(1, '192.168.0.24', 'frsajche', '2021-06-10 17:31:26', 'El usuario frsajche ha iniciado sesión.'),
(2, '192.168.0.24', 'frsajche', '2021-06-10 17:42:28', 'El usuario frsajche ha iniciado sesión.'),
(3, '192.168.0.24', 'frsajche', '2021-06-10 17:43:36', 'El usuario frsajche ha iniciado sesión.'),
(4, '192.168.0.24', 'frsajche', '2021-06-10 17:44:26', 'El usuario frsajche ha iniciado sesión.'),
(5, '192.168.0.24', 'frsajche', '2021-06-10 17:45:20', 'El usuario frsajche ha iniciado sesión.'),
(6, '192.168.0.24', 'frsajche', '2021-06-10 17:45:45', 'El usuario frsajche ha iniciado sesión.'),
(7, '192.168.0.24', 'frsajche', '2021-06-10 17:48:16', 'El usuario frsajche ha iniciado sesión.'),
(8, '192.168.0.24', 'frsajche', '2021-06-10 17:58:54', 'El usuario frsajche ha iniciado sesión.'),
(9, '192.168.0.24', 'frsajche', '2021-06-10 18:01:28', 'El usuario frsajche ha iniciado sesión.'),
(10, '192.168.0.24', 'frsajche', '2021-06-10 18:02:37', 'El usuario frsajche ha iniciado sesión.'),
(11, '192.168.0.24', 'usuario', '2021-06-10 18:02:42', 'Se ha eliminado al usuario, o mas bien, ha cambiado de estado a INACTIVO.'),
(12, '192.168.0.24', 'frsajche', '2021-06-10 18:04:45', 'El usuario frsajche ha iniciado sesión.'),
(13, '192.168.0.24', 'frsajche', '2021-06-10 18:05:27', 'El usuario frsajche ha iniciado sesión.'),
(14, '192.168.0.24', 'usuario', '2021-06-10 18:05:41', 'Se ha eliminado al usuario, o mas bien, ha cambiado de estado a INACTIVO.'),
(15, '192.168.0.24', 'frsajche', '2021-06-10 18:06:30', 'El usuario frsajche ha iniciado sesión.'),
(16, '192.168.0.24', 'usuario', '2021-06-10 18:06:38', 'Se ha eliminado al usuario, o mas bien, ha cambiado de estado a INACTIVO.'),
(17, '192.168.0.24', 'frsajche', '2021-06-10 18:08:29', 'El usuario frsajche ha iniciado sesión.'),
(18, '192.168.0.24', 'usuario', '2021-06-10 18:08:44', 'Se ha modificado los datos del usuario: Alejandro Barrios'),
(19, '192.168.0.24', 'frsajche', '2021-06-10 18:10:56', 'El usuario frsajche ha iniciado sesión.'),
(20, '192.168.0.24', 'usuario', '2021-06-10 18:11:08', 'Se ha eliminado al usuario, o mas bien, ha cambiado de estado a INACTIVO.'),
(21, '192.168.0.24', 'frsajche', '2021-06-10 18:18:02', 'El usuario frsajche ha iniciado sesión.'),
(22, '192.168.0.24', 'usuario', '2021-06-10 18:19:50', 'Se ha eliminado al usuario, o mas bien, ha cambiado de estado a INACTIVO.'),
(23, '192.168.0.24', 'frsajche', '2021-06-10 18:21:07', 'El usuario frsajche ha iniciado sesión.'),
(24, '192.168.0.24', 'usuario', '2021-06-10 18:21:22', 'Se ha modificado los datos del usuario: Orlando Orozco'),
(25, '192.168.0.24', 'usuario', '2021-06-10 18:21:28', 'Se ha modificado los datos del usuario: Alejandro Barrios'),
(26, '192.168.0.24', 'usuario', '2021-06-10 18:21:33', 'Se ha modificado los datos del usuario: Francis Hernandez'),
(27, '192.168.0.24', 'frsajche', '2021-06-10 18:32:09', 'El usuario frsajche ha iniciado sesión.'),
(28, '192.168.0.24', 'frsajche', '2021-06-10 18:33:17', 'El usuario frsajche ha iniciado sesión.'),
(29, '192.168.0.24', 'frsajche', '2021-06-10 18:36:48', 'El usuario frsajche ha iniciado sesión.'),
(30, '192.168.0.24', 'frsajche', '2021-06-10 18:37:22', 'El usuario frsajche ha iniciado sesión.'),
(31, '192.168.0.24', 'frsajche', '2021-06-10 18:38:43', 'El usuario frsajche ha iniciado sesión.'),
(32, '192.168.0.24', 'frsajche', '2021-06-10 18:42:06', 'El usuario frsajche ha iniciado sesión.'),
(33, '192.168.0.24', 'frsajche', '2021-06-10 18:43:19', 'El usuario frsajche ha iniciado sesión.'),
(34, '192.168.0.24', 'frsajche', '2021-06-10 18:46:10', 'El usuario frsajche ha iniciado sesión.'),
(35, '192.168.0.24', 'frsajche', '2021-06-10 18:48:07', 'El usuario frsajche ha iniciado sesión.'),
(36, '192.168.0.24', 'frsajche', '2021-06-10 18:54:12', 'El usuario frsajche ha iniciado sesión.'),
(37, '192.168.0.24', 'francis', '2021-06-10 18:57:02', 'El usuario francis ha iniciado sesión.'),
(38, '192.168.0.24', 'frsajche', '2021-06-10 19:02:49', 'El usuario frsajche ha iniciado sesión.'),
(39, '192.168.0.24', 'frsajche', '2021-06-10 19:03:43', 'El usuario frsajche ha iniciado sesión.'),
(40, '192.168.0.24', 'frsajche', '2021-06-10 19:04:27', 'El usuario frsajche ha iniciado sesión.'),
(41, '192.168.0.24', 'francis', '2021-06-10 19:10:14', 'El usuario francis ha iniciado sesión.'),
(42, '192.168.0.24', 'francis', '2021-06-10 19:11:19', 'El usuario francis ha iniciado sesión.'),
(43, '192.168.0.24', 'frsajche', '2021-06-10 19:12:35', 'El usuario frsajche ha iniciado sesión.'),
(44, '192.168.0.24', 'frsajche', '2021-06-10 19:12:57', 'El usuario frsajche ha iniciado sesión.'),
(45, '192.168.0.24', 'frsajche', '2021-06-10 19:14:30', 'El usuario frsajche ha iniciado sesión.'),
(46, '192.168.0.24', 'frsajche', '2021-06-10 19:14:50', 'El usuario frsajche ha iniciado sesión.'),
(47, '192.168.0.24', 'frsajche', '2021-06-10 19:15:25', 'El usuario frsajche ha iniciado sesión.'),
(48, '192.168.0.24', 'francis', '2021-06-10 19:15:31', 'El usuario francis ha iniciado sesión.'),
(49, '192.168.0.24', 'francis', '2021-06-10 19:21:33', 'El usuario francis ha iniciado sesión.'),
(50, '192.168.0.24', 'francis', '2021-06-10 19:24:28', 'El usuario francis ha iniciado sesión.'),
(51, '192.168.0.24', 'frsajche', '2021-06-10 19:44:01', 'El usuario frsajche ha iniciado sesión.'),
(52, '192.168.0.24', 'francis', '2021-06-10 19:44:13', 'El usuario francis ha iniciado sesión.'),
(53, '192.168.0.24', 'francis', '2021-06-10 19:53:54', 'El usuario francis ha iniciado sesión.'),
(54, '192.168.0.24', 'francis', '2021-06-10 19:55:20', 'El usuario francis ha iniciado sesión.'),
(55, '192.168.0.24', 'francis', '2021-06-10 20:03:16', 'El usuario francis ha iniciado sesión.'),
(56, '192.168.0.24', 'francis', '2021-06-10 20:08:34', 'El usuario francis ha iniciado sesión.'),
(57, '192.168.0.24', 'francis', '2021-06-10 20:09:42', 'El usuario francis ha iniciado sesión.'),
(58, '192.168.0.24', 'francis', '2021-06-10 20:17:11', 'El usuario francis ha iniciado sesión.'),
(59, '192.168.0.24', 'francis', '2021-06-10 20:18:23', 'El usuario francis ha iniciado sesión.'),
(60, '192.168.0.24', 'francis', '2021-06-10 20:19:03', 'El usuario francis ha iniciado sesión.'),
(61, '192.168.0.24', 'francis', '2021-06-10 20:21:39', 'El usuario francis ha iniciado sesión.'),
(62, '192.168.0.24', 'francis', '2021-06-10 20:25:28', 'El usuario francis ha iniciado sesión.'),
(63, '192.168.0.24', 'francis', '2021-06-10 20:30:25', 'El usuario francis ha iniciado sesión.'),
(64, '192.168.0.24', 'francis', '2021-06-10 20:32:40', 'El usuario francis ha iniciado sesión.'),
(65, '192.168.0.24', 'francis', '2021-06-10 20:36:39', 'El usuario francis ha iniciado sesión.'),
(66, '192.168.0.24', 'francis', '2021-06-10 20:39:39', 'El usuario francis ha iniciado sesión.'),
(67, '192.168.0.24', 'francis', '2021-06-10 20:41:56', 'El usuario francis ha iniciado sesión.'),
(68, '192.168.0.24', 'francis', '2021-06-10 20:49:25', 'El usuario francis ha iniciado sesión.'),
(69, '192.168.0.24', 'francis', '2021-06-10 20:49:51', 'El usuario francis ha iniciado sesión.'),
(70, '192.168.0.24', 'francis', '2021-06-10 21:40:36', 'El usuario francis ha iniciado sesión.'),
(71, '192.168.0.24', 'abarrios', '2021-06-10 21:40:57', 'El usuario abarrios ha iniciado sesión.'),
(72, '192.168.0.24', 'francis', '2021-06-10 21:44:33', 'El usuario francis ha iniciado sesión.'),
(73, '192.168.0.24', 'francis', '2021-06-10 21:45:26', 'El usuario francis ha iniciado sesión.'),
(74, '192.168.0.24', 'francis', '2021-06-10 21:50:38', 'El usuario francis ha iniciado sesión.'),
(75, '192.168.0.24', 'francis', '2021-06-10 21:53:10', 'El usuario francis ha iniciado sesión.'),
(76, '192.168.0.24', 'francis', '2021-06-10 21:55:59', 'El usuario francis ha iniciado sesión.'),
(77, '192.168.0.24', 'francis', '2021-06-10 21:59:32', 'El usuario francis ha iniciado sesión.'),
(78, '192.168.0.24', 'francis', '2021-06-10 22:06:51', 'El usuario francis ha iniciado sesión.'),
(79, '192.168.0.24', 'Francis', '2021-06-10 22:07:19', 'Ha pasado al nivel INTERMEDIO'),
(80, '192.168.0.24', 'francis', '2021-06-10 22:07:43', 'El usuario francis ha iniciado sesión.'),
(81, '192.168.0.24', 'Francis', '2021-06-10 22:07:54', 'Ha pasado al nivel AVANZADO'),
(82, '192.168.0.24', 'francis', '2021-06-10 22:08:08', 'El usuario francis ha iniciado sesión.'),
(83, '192.168.0.24', 'francis', '2021-06-10 22:10:35', 'El usuario francis ha iniciado sesión.'),
(84, '192.168.0.24', 'Francis', '2021-06-10 22:11:03', 'Ha finalizado exitosamente el juego.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_accion`
--

CREATE TABLE `tbl_accion` (
  `id_accion` int(10) NOT NULL,
  `nombre_accion` varchar(150) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_accion`
--

INSERT INTO `tbl_accion` (`id_accion`, `nombre_accion`) VALUES
(1, 'sumar numeros'),
(2, 'restar numeros'),
(3, 'multiplicar numeros'),
(4, 'dividir numeros'),
(5, 'historia guatemalteca'),
(6, 'adivinar numero aleatorio'),
(7, 'traducir palabra 1'),
(8, 'traducir palabra 2'),
(9, 'traducir palabra 3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_estado`
--

CREATE TABLE `tbl_estado` (
  `id_estado` int(10) NOT NULL,
  `nombre_estado` varchar(75) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion_estado` varchar(400) COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_estado`
--

INSERT INTO `tbl_estado` (`id_estado`, `nombre_estado`, `descripcion_estado`) VALUES
(1, 'Activo', NULL),
(2, 'Inactivo', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_score`
--

CREATE TABLE `tbl_score` (
  `id_score` int(10) NOT NULL,
  `punteo_score` int(10) NOT NULL,
  `fk_id_usuario` int(10) NOT NULL,
  `fk_nivel_usuario` int(10) NOT NULL,
  `fk_accion` int(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_score`
--

INSERT INTO `tbl_score` (`id_score`, `punteo_score`, `fk_id_usuario`, `fk_nivel_usuario`, `fk_accion`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 2, 1, '2021-06-10 22:06:57', NULL),
(2, 1, 2, 2, 2, '2021-06-10 22:07:03', NULL),
(3, 1, 2, 2, 3, '2021-06-10 22:07:11', NULL),
(4, 1, 2, 2, 4, '2021-06-10 22:07:18', NULL),
(5, 1, 2, 3, 5, '2021-06-10 22:07:53', NULL),
(6, 1, 2, 4, 6, '2021-06-10 22:10:43', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_tipo_usuario`
--

CREATE TABLE `tbl_tipo_usuario` (
  `id_tipo_usuario` int(10) NOT NULL,
  `nombre_tipo_usuario` varchar(75) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion_tipo_usuario` varchar(400) COLLATE latin1_spanish_ci DEFAULT NULL,
  `puntaje` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_tipo_usuario`
--

INSERT INTO `tbl_tipo_usuario` (`id_tipo_usuario`, `nombre_tipo_usuario`, `descripcion_tipo_usuario`, `puntaje`) VALUES
(1, 'Administrador', NULL, 0),
(2, 'Principiante', NULL, 4),
(3, 'Intermedio', NULL, 1),
(4, 'Avanzado', NULL, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_usuario`
--

CREATE TABLE `tbl_usuario` (
  `id_usuario` int(10) NOT NULL,
  `nombre_usuario` varchar(75) COLLATE latin1_spanish_ci NOT NULL,
  `apellido_usuario` varchar(75) COLLATE latin1_spanish_ci NOT NULL,
  `edad_usuario` int(3) NOT NULL,
  `nick` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `password` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fk_id_estado` int(10) NOT NULL,
  `fk_id_tipo_usuario` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_usuario`
--

INSERT INTO `tbl_usuario` (`id_usuario`, `nombre_usuario`, `apellido_usuario`, `edad_usuario`, `nick`, `password`, `fk_id_estado`, `fk_id_tipo_usuario`) VALUES
(1, 'Roberto', 'Sajche', 29, 'frsajche', 'Abc$2020', 1, 1),
(2, 'Francis', 'Hernandez', 32, 'francis', '12345', 1, 4),
(3, 'Alejandro', 'Barrios', 29, 'abarrios', '12345', 1, 3),
(4, 'Orlando', 'Orozco', 28, 'oorozco', '12345', 1, 4);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `tbl_accion`
--
ALTER TABLE `tbl_accion`
  ADD PRIMARY KEY (`id_accion`);

--
-- Indices de la tabla `tbl_estado`
--
ALTER TABLE `tbl_estado`
  ADD PRIMARY KEY (`id_estado`);

--
-- Indices de la tabla `tbl_score`
--
ALTER TABLE `tbl_score`
  ADD PRIMARY KEY (`id_score`);

--
-- Indices de la tabla `tbl_tipo_usuario`
--
ALTER TABLE `tbl_tipo_usuario`
  ADD PRIMARY KEY (`id_tipo_usuario`);

--
-- Indices de la tabla `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `fk_estado_usuario` (`fk_id_estado`),
  ADD KEY `fk_tipo_usuario` (`fk_id_tipo_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT de la tabla `tbl_accion`
--
ALTER TABLE `tbl_accion`
  MODIFY `id_accion` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `tbl_estado`
--
ALTER TABLE `tbl_estado`
  MODIFY `id_estado` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tbl_score`
--
ALTER TABLE `tbl_score`
  MODIFY `id_score` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `tbl_tipo_usuario`
--
ALTER TABLE `tbl_tipo_usuario`
  MODIFY `id_tipo_usuario` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  MODIFY `id_usuario` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  ADD CONSTRAINT `fk_estado_usuario` FOREIGN KEY (`fk_id_estado`) REFERENCES `tbl_estado` (`id_estado`),
  ADD CONSTRAINT `fk_tipo_usuario` FOREIGN KEY (`fk_id_tipo_usuario`) REFERENCES `tbl_tipo_usuario` (`id_tipo_usuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
